# 1 Understanding the Domain
## 1.1 Introducing Domain-Driven Design
- 'Garbage in, garbage out'
  - Bad input by unclear requirements or a bad design
  - How to minimize the 'garbage in' part
    - by using DDD: a design approach focused on clear communication / shared domain knowledge
- In this chapter
  - The principles of DDD, how to apply them briefly
    - how DDD works
    - how it is different from database-driven one / object-oriented one
- DDD is not appropriate for all software, especially for where developers have to collaborate with other nontechnical teams
### 1.1.1 The Importance of a Shared Model
- **Obviously**, the developers' understanding is incomplete / distorted(aka garbage in) => garbage out
- How can we ensure that **developers do understand** the problem?
  - Solution 1 : by using written specifications / requirements to try to capture all the details of problem
    - Shortage: creates distance from "domain experts" to "development team"
      - The understanding mismatch can be fatal
    - ex) Goyosoge oechim
  - Solution 2 : to eliminate the intermediaries by introducing a feedback loop b2w dev and experts(aka agile)
    - Shortage: still translating the domain expert's mental model into code
      - Any translation result in distortion and loss of important subtleties
      - If the code doesn't correspond to the domain concepts, future developers can likely misunderstand what's needed and introduce errors.
  - Solution 3 : What if all share the same model?
    - No translation from the domain expert's requirements
    - The code is designed to reflect the shared model directly.
    - Advantages
      - Faster time to market
      - More business value
        - happier customers and less chance of going offtrack
      - Less waste
        - Clearer requirements
          - reduce time wasted in misunderstanding and rework
          - reveals which components have higher priority
      - Easier maintenance and evolution
        - making changes to be easier / less error-prone
        - new team members are able to come up to speed faster
- How to create a shared model => the following four chapters
  - Focus on business events / workflows rather than data structures
  - Partition the problem domain into smaller subdomains
  - Create a model of each subdomain in the solution
  - Develop a common language(aka ubiquitous language) shared between everyone, used everywhere in the code
### 1.1.2 Understanding the Domain Through Business Events
- Business process = a series of data transformations
  - The value of the business is created in the transformation.
  - **critically important** to understand how these transformations work, how they relate to each other
- What causes an employee to start working with that data, i.e. adding value?
  - **Domain events**: from outside, time-based, by observation
    - the starting point for the business processes
    - always written in past tense: sth happened
      - the fact not to be changed
#### Using Event Storming to Discover the Domain
- Event Storming: a collaborative process for discovering [business events / associated workflows]
  - The attendees should include **anyone who has questions and anyone who has answers**.
    - The idea is **to get all the attendees to participate** in 
      - posting **what they know**
      - asking questions about **what they don't know**
    - The flow of event storming
      - Post business events
      - Post notes summarizing the workflows triggered by these events
      - Those lead to other business events being created.
  - for more detail, refer to EventStorming book by Alberto Brandolini
#### Discovering the Domain: An Order-Taking System
#### Expanding the Events to the Edges
#### Documenting Commands
### 1.1.3 Partitioning the Domain into Subdomains
### 1.1.4 Creating a Solution Using Bounded Contexts
### 1.1.5 Creating a Ubiquitous Language
### 1.1.6 Summarizing the Concepts of Domain-Driven Design
### 1.1.7 Wrapping Up